<%@ Page Title="Tạo bản đồ - Hệ thống thông tin tỉnh Thừa Thiên Huế" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<body class="page-header-fixed page-sidebar-fixed page-sidebar-closed-hide-logo">
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner">
            <div class="page-logo" style="width: 300px;">
                <a href="/Default.aspx">
                    <img class="logo-default" alt="logo" src="Assets/images/logo.png">
                </a>
            </div>
            <div id="idToolBar">
                <div class="page-actions" id="contentToolbar">
                    <h4 class="company-head-title" id="mapname">
                     <%--   <div><a class="linkbuttonMapInfo">Tra cứu thông tin đất đai</a></div>--%>
                        <div><asp:LinkButton ID="LinkButton1" CssClass="linkbuttonMapInfo" runat="server">Tra cứu thông tin đất đai</asp:LinkButton></div>
                    </h4>
                     
                    <div class="btn-group first">
                        <li data-close-others="true" data-hover="" data-toggle="dropdown" class="btn theme-haze btn-sm dropdown-toggle div-active">
                            <span class="showwr">Bản đồ </span><i class="fa fa-angle-down"></i>
                        </li>
                        <ul role="menu" class="dropdown-menu">
                            <li>
                                <a class="geditor-map" id="geditor_OpenMap">
                                <i class="g-font16 g-menu-openmap"></i>Mở bản đồ</a>
                            </li>
                        </ul>
                    </div>
                         
                    <div class="btn-group">
                        <li data-close-others="true" id="tool_congcu" data-hover="" data-toggle="dropdown" class="btn theme-haze btn-sm dropdown-toggle div-active">
                            <span class="showwr">Công cụ</span><i class="fa fa-angle-down"></i>
                        </li>
                        <ul role="menu" class="dropdown-menu">
                            <li class="geditor-toolManager">
                                <a id="g-tools-chart" href="#">
                                    <i class="g-font16 g-menu-openchart"></i>Biểu đồ</a>
                            </li>
                            <li class="geditor-toolManager"></li>
                            <li class="geditor-toolManager">
                                <a id="g-tools-topology" href="#">
                                    <i class="g-font16 g-menu-topology"></i>Kiểm tra quan hệ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <li data-close-others="true" id="divAdvanceSearch" data-hover="" data-toggle="dropdown" class="btn theme-haze btn-sm dropdown-toggle div-active">
                            <span class="">Tìm kiếm</span><i class="fa fa-angle-down"></i>
                        </li>
                        <ul role="menu" class="dropdown-menu">
                            <li class="padding410">
                                <label href="#" class=" placeName">
                                    <h4 id="div_|_31745" class="default-color left gevnsearch-map"><span class="g-font16 g-menu-searchlayer left cls marginR15"></span><span class="ico" title="Thửa đất">Thửa đất</span></h4>
                                </label>
                            </li>
                            <li class="padding410">
                                <label href="#" class=" placeName">
                                    <h4 id="div_|_31746" class="default-color left gevnsearch-map"><span class="g-font16 g-menu-searchlayer left cls marginR15"></span><span class="ico" title="Đối tượng chiếm đất">Đối tượng chiếm đất</span></h4>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="page-top">
                <div id="idDivSearch" class="search-form open" style="">
                    <div id="contentDivSearch">
                        <div data-original-title="Tìm kiếm theo địa điểm" data-html="true" data-placement="bottom" class="search-change tooltips objectSearch">
                            <i class="fa fa-dot-circle-o changeIcon"></i>
                            <i class="fa fa-angle-down small"></i>
                        </div>
                        <div class="input-group">
                            <%--<input type="text" id="gMapInputTextSearch" data-typesearch="SearchObject" class="form-control input-sm" placeholder="Nhập thông tin đối tượng...">--%>
                            <asp:TextBox id="gMapInputTextSearch" data-typesearch="SearchObject" CssClass="form-control input-sm" placeholder="Nhập thông tin đối tượng..." runat="server"/>
                            <span class="input-group-btn">
                                <%--<a id="gMapButtonSearch" class="btn submit"><i class="fa fa-search"></i></a>--%>
                                <asp:LinkButton ID="gMapButtonSearch"  class="btn submit" CssClass="fa fa-search" runat="server"></asp:LinkButton>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="center-panel">
        <div class="west-panel-body height100">
            <div id="west-tabpanel" class="x-panel MainPanel x-fit-item x-panel-default width100 height100">
                <div id="tabbar-1010" class="x-tab-bar x-tab-bar-default x-top x-tab-bar-top x-tab-bar-default-top x-horizontal x-tab-bar-horizontal x-tab-bar-default-horizontal x-docked-top x-tab-bar-docked-top x-tab-bar-default-docked-top x-unselectable width100">
                    <div id="tabbar-1010-body" class="x-tab-bar-body x-tab-bar-body-top x-tab-bar-body-default-top x-tab-bar-body-horizontal x-tab-bar-body-default-horizontal x-tab-bar-body-default x-tab-bar-body-default-top x-tab-bar-body-default-horizontal x-tab-bar-body-default-docked-top x-box-layout-ct width100">
                        <div id="tabbar-1010-innerCt" class="x-box-inner x-horizontal-box-overflow-body width100" role="presentation">
                            <div id="headingOne" class="x-tab x-box-item x-tab-default x-active x-tab-active x-tab-default-active x-top-active x-tab-top-active x-tab-default-top-active x-noicon x-tab-noicon x-tab-default-noicon x-top x-tab-top x-tab-default-top control-home" data-tabkt="featuresClassPanelOne">
                                <em id="tab-1016-btnWrap">
                                    <button id="tab-1016-btnEl" type="button" class="x-tab-center" hidefocus="true" role="button" autocomplete="off">
                                        <span id="tab-1016-btnInnerEl" class="x-tab-inner" style="">Lớp bản đồ</span><span id="tab-1016-btnIconEl" class="x-tab-icon "></span>
                                    </button>
                                </em>
                            </div>
                            <div id="headingTwo" class="x-tab x-box-item x-tab-default x-noicon x-tab-noicon x-tab-default-noicon x-top x-tab-top x-tab-default-top control-home" style="left: 82px;" data-tabkt="featureDetailPanelTwo">
                                <em id="tab-1017-btnWrap">
                                    <button id="tab-1017-btnEl" type="button" class="x-tab-center" hidefocus="true" role="button" autocomplete="off">
                                        <span id="tab-1017-btnInnerEl" class="x-tab-inner" style="">Chú giải</span><span id="tab-1017-btnIconEl" class="x-tab-icon "></span>
                                    </button>
                                </em>
                            </div>
                            <div id="headingThree" class="x-tab x-box-item x-tab-default x-noicon x-tab-noicon x-tab-default-noicon x-top x-tab-top x-tab-default-top control-home" style="left: 147px;" data-tabkt="featureDetailPanelThree">
                                <em id="tab-1018-btnWrap">
                                    <button id="tab-1018-btnEl" type="button" class="x-tab-center" hidefocus="true" role="button" autocomplete="off">
                                        <span id="tab-1018-btnInnerEl" class="x-tab-inner" style="">Thuộc tính</span><span id="tab-1018-btnIconEl" class="x-tab-icon "></span>
                                    </button>
                                </em>
                            </div>
                            <div id="headingFour" class="x-tab x-box-item x-tab-default x-noicon x-tab-noicon x-tab-default-noicon x-top x-tab-top x-tab-default-top control-home" style="left: 224px;" data-tabkt="featureDetailPanelFour">
                                <em id="tab-1019-btnWrap">
                                    <button id="tab-1019-btnEl" type="button" class="x-tab-center" hidefocus="true" role="button" autocomplete="off">
                                        <span id="tab-1019-btnInnerEl" class="x-tab-inner" style="">Kết quả</span><span id="tab-1019-btnIconEl" class="x-tab-icon "></span>
                                    </button>
                                </em>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="west-tabpanel-body" class="x-panel-body x-panel-body-default x-panel-body-default x-layout-fit panel-scroll">
                    <div id="featuresClassPanelOne" class="x-panel x-tabpanel-child x-panel-default headingOne cozy-all">
                        <div id="toolbar-1011" class="x-toolbar x-toolbar-default x-docked-top x-toolbar-docked-top x-toolbar-default-docked-top x-docked-noborder-top x-docked-noborder-right x-docked-noborder-left">
                            <div id="toolbar-1011-innerCt">
                                <ul class="list-inline" id="toolbar-1011-targetEl">
                                    <li class="active">
                                        <span role="button" class="tooltips" title="" data-original-title="Hiển thị vừa khung theo lớp bản đồ" data-html="true" data-placement="bottom">
                                            <span id="btnZoomToLayer-btnIconEl" class="x-btn-icon g-font16 g-tools-zoomtolayer"></span>
                                        </span>
                                    </li>
                                    <li>
                                        <span role="button" class="tooltips" title="" data-original-title="Bảng thuộc tính" data-html="true" data-placement="bottom">
                                            <span id="btnTableAttribute-btnIconEl" class="x-btn-icon g-font16 g-tools-fa-tableattribute"></span>
                                        </span>
                                    </li>
                                    <li class="active">
                                        <span role="button" class="tooltips" title="" data-original-title="Xem bản đồ dưới dạng 3D" data-html="true" data-placement="bottom">
                                            <span id="btnView3D-btnIconEl" class="x-btn-icon g-font16 g-tools-view3d"></span>
                                        </span>
                                    </li>
                                    <li class="active">
                                        <span role="button" class="tooltips" title="" data-original-title="Mở lại bản đồ" data-html="true" data-placement="bottom">
                                            <span id="btnReloadMap-btnIconEl" class="x-btn-icon g-font16 g-tools-reopen"></span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="featuresClassPanel-body" class="x-panel-body x-panel-body-default x-panel-body-default x-layout-fit x-docked-noborder-right x-docked-noborder-bottom x-docked-noborder-left borderTop">
                            <div class="panel-group">
                                <div class="panel borderNone">
                                    <div class="x-grid-cell-inner active" id="heading1">
                                        <a role="button">
                                            <span class="open"></span>
                                            <span class="folder"></span>
                                            Lớp nền
                                        </a>
                                    </div>
                                    <div id="collapse1" class="shown">
                                        <div class="x-grid-cell-treecolumn">
                                            <a href="#" class="active">
                                                <span class="round"></span>
                                                <span class="earth"></span>
                                                Bản đồ hành chính Huế
                                            </a>
                                            <a href="#">
                                                <span class="round"></span>
                                                <span class="earth"></span>
                                                Google
                                            </a>
                                            <a href="#">
                                                <span class="round"></span>
                                                <span class="earth"></span>
                                                Vệ tinh
                                            </a>
                                            <a href="#">
                                                <span class="round"></span>
                                                <span class="earth"></span>
                                                Không nền
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel borderNone">
                                    <div class="x-grid-cell-inner active" id="heading2">
                                        <a role="button">
                                            <span class="open"></span>
                                            <span class="folder"></span>
                                            Lớp chuyên đề
                                        </a>
                                    </div>
                                    <div id="collapse2" class="shown">
                                        <div class="x-grid-cell-treecolumn-check">
                                            <a href="#" class="active">
                                                <span class="check"></span>
                                                <span class="sand"></span>
                                                Thửa đất
                                            </a>
                                            <a href="#">
                                                <span class="check"></span>
                                                <span class="sand"></span>
                                                Đối tượng chiếm đất
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel borderNone">
                                    <div class="x-grid-cell-inner" id="heading3">
                                        <a role="button">
                                            <span class="open"></span>
                                            <span class="folder"></span>
                                            Lớp dữ liệu
                                        </a>
                                    </div>
                                    <div id="collapse3" class="">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="featureDetailPanelTwo" class="headingTwo cozy-all">
                        <div class="panel borderNone marginB0">
                            <div class="x-grid-cell-inner-pti active" id="headingFour">
                                <a role="button">
                                    <span class="open"></span>
                                    Thửa đất
                                </a>
                            </div>
                            <div id="collapseFour" class="shown">
                                <div class="gClient_Layer_Canvas_243_legend_rule_default  text-red">
                                    <span class="square" style="background: #EBB4B5;"></span>
                                    Hiện trạng sử dụng đất
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FFFFFE;"></span>
                                    Ma_LoaiDat = 'BCS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FDF0B6;"></span>
                                    Ma_LoaiDat = 'BHK'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F84E45;"></span>
                                    Ma_LoaiDat = 'CAN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'CLN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F96250;"></span>
                                    Ma_LoaiDat = 'CQP'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DBV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DCH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DDT'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'DGD'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DKV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DNL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DSH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #B1FFFF;"></span>
                                    Ma_LoaiDat = 'DTL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F6AAA0;"></span>
                                    Ma_LoaiDat = 'DTS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DTT'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #EBB4B5;"></span>
                                    Hiện trạng sử dụng đất
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FFFFFE;"></span>
                                    Ma_LoaiDat = 'BCS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FDF0B6;"></span>
                                    Ma_LoaiDat = 'BHK'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F84E45;"></span>
                                    Ma_LoaiDat = 'CAN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'CLN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F96250;"></span>
                                    Ma_LoaiDat = 'CQP'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DBV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DCH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DDT'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'DGD'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DKV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DNL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DSH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #B1FFFF;"></span>
                                    Ma_LoaiDat = 'DTL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F6AAA0;"></span>
                                    Ma_LoaiDat = 'DTS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DTT'
                                </div>
                            </div>
                        </div>
                        <div class="panel borderNone marginB0">
                            <div class="x-grid-cell-inner-pti active" id="headingFive">
                                <a role="button">
                                    <span class="open"></span>
                                    Đối tượng chiếm đất
                                </a>
                            </div>
                            <div id="collapseFive" class="shown">
                                <div class="gClient_Layer_Canvas_243_legend_rule_default text-red">
                                    <span class="square" style="background: #EBB4B5;"></span>
                                    Hiện trạng sử dụng đất
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FFFFFE;"></span>
                                    Ma_LoaiDat = 'BCS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FDF0B6;"></span>
                                    Ma_LoaiDat = 'BHK'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F84E45;"></span>
                                    Ma_LoaiDat = 'CAN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'CLN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F96250;"></span>
                                    Ma_LoaiDat = 'CQP'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DBV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DCH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DDT'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'DGD'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DKV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DNL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DSH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #B1FFFF;"></span>
                                    Ma_LoaiDat = 'DTL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F6AAA0;"></span>
                                    Ma_LoaiDat = 'DTS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DTT'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #EBB4B5;"></span>
                                    Hiện trạng sử dụng đất
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FFFFFE;"></span>
                                    Ma_LoaiDat = 'BCS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FDF0B6;"></span>
                                    Ma_LoaiDat = 'BHK'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F84E45;"></span>
                                    Ma_LoaiDat = 'CAN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'CLN'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F96250;"></span>
                                    Ma_LoaiDat = 'CQP'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DBV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DCH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DDT'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FCD2A1;"></span>
                                    Ma_LoaiDat = 'DGD'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DKV'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DNL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DSH'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #B1FFFF;"></span>
                                    Ma_LoaiDat = 'DTL'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #F6AAA0;"></span>
                                    Ma_LoaiDat = 'DTS'
                                </div>
                                <div class="gClient_Layer_Canvas_243_legend_rule_default">
                                    <span class="square" style="background: #FBAAA0;"></span>
                                    Ma_LoaiDat = 'DTT'
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="featureDetailPanelThree" class="headingThree cozy-all">
                        <div class="clearfix top">
                            <div class="name">Thửa đất</div>
                            <div class="right">
                                <div class="icon">
                                    <span id="button-1072-btnIconEl" class="x-btn-icon icon-list"></span>
                                </div>
                                <div class="dropdown icon">
                                    <span class="clearfix" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span id="splitbutton-1073-btnIconEl" class="x-btn-icon icon-list"></span>
                                        <span class="down"></span>
                                    </span>
                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
                                        <li>Tạo bản đồ</li>
                                        <li>Xuất Excel</li>
                                        <li>In</li>
                                    </ul>
                                </div>
                                <div class="icon">
                                    <span id="button-1079-btnIconEl" class="x-btn-icon icon-maximize"></span>
                                </div>
                            </div>
                        </div>
                        <div class="conten">
                            <div class="clearfix unit">
                                <div class="left">Mã xã:</div>
                                <div class="text">Xã Phong Sơn</div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Số tờ:</div>
                                <div class="text">3</div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Số thửa:</div>
                                <div class="text">71</div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Diện tích (m2):</div>
                                <div class="text">962556.2</div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Diện tích pháp lý (m2):</div>
                                <div class="text"></div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Địa chỉ:</div>
                                <div class="text">xã Phong Sơn, huyện Phong Điền, tỉnh Thừa Thiên Huế</div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Loại đất:</div>
                                <div class="text">RSX</div>
                            </div>
                            <div class="clearfix unit">
                                <div class="left">Tình trạng pháp lý:</div>
                                <div class="text"></div>
                            </div>
                        </div>
                        <div class="bottom">
                            <span class="clearfix action">
                                <span id="button-1062-btnIconEl" class="x-btn-icon g-font16 g-tools-delete"></span>
                                Hủy
                            </span>

                        </div>
                    </div>
                    <div id="featureDetailPanelFour" class="headingFour cozy-all">
                        Kết quả đê
                    </div>
                </div>
            </div>
        </div>
        <div id="west-panel-splitter">
            <span class="control"></span>
        </div>
        <div class="panel-1020">
            <div id="toolTopPanel">
                <div id="toolTopPanel-body">
                    <div id="right-wap" class="page-head">
                        <div class="toolbarCenter toolbarCenterBorder">
                            <div class="toolsBarLeft">
                                <div class="listIcon">
                                    <div class="btn-group">
                                        <li class="item">
                                            <a class="itemClick2" id="id-panel-arrow" href="#">
                                                <span style="font-size: 13px; margin-top: 0px" class="id-panel-arrow icon-16  itemEffect noDropDown iconPackArrow"></span><span class="default-color"></span>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="btn-group">
                                        <li data-toggle="dropdown" data-hover="" data-close-others="true" class="dropdown-toggle">
                                            <a href="#" title="" class="tooltips" data-original-title="Tiện ích bản đồ" data-html="true" data-placement="bottom" style="text-decoration: none;"><i class="g-font16 g-tools-utilities"><i class="fa fa-angle-down"></i></i>
                                            </a>
                                        </li>
                                        <ul role="menu" class="dropdown-menu">
                                            <li>
                                                <a href="#" id="btTienIchDoDienTich">
                                                    <i class="g-font16 g-tools-mesure"></i>Đo diện tích </a>
                                            </li>
                                            <li>
                                                <a href="#" id="btTienIchDoChieuDai">
                                                    <i class="g-font16 g-tools-area"></i>Đo chiều dài</a>
                                            </li>
                                            <li>
                                                <a href="#" id="btTienIchThongTinDiem">
                                                    <i class="g-font16 g-tools-locationinfo"></i>Lấy thông tin vị trí</a>
                                            </li>
                                            <li>
                                                <a href="#" id="btnScreenMap">
                                                    <i class="g-font16 g-tools-printmap"></i>Chụp màn hình</a>
                                            </li>
                                            <li>
                                                <a href="#" id="btnExportMap">
                                                    <i class="g-font16 g-tools-exportmap"></i>Xuất bản đồ</a>
                                            </li>
                                            <li>
                                                <a href="#" id="btnPrintMap">
                                                    <i class="g-font16 g-tools-exportmap"></i>In bản đồ</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="btn-group">
                                        <li>
                                            <a href="#" id="featureinfo" title="" class="tooltips selected" data-original-title="Xem thông tin đối tượng" data-html="true" data-placement="bottom">
                                                <i class="fa fa-info"></i>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="btn-group">
                                        <li>
                                            <a href="#" id="featureselected" title="" class="tooltips" data-original-title="Chọn một đối tượng" data-html="true" data-placement="bottom">
                                                <i class="g-tools-select"></i>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="btn-group">
                                        <li>
                                            <a href="#" id="areaselected" class="tooltips" data-original-title="Chọn nhiều đối tượng" data-html="true" data-placement="bottom" title="">
                                                <i class="g-font16 g-tools-selectall"></i>
                                            </a>
                                        </li>
                                    </div>
                                    <div class="btn-group">
                                        <li>
                                            <a href="#" id="pan" class="tooltips" data-original-title="Dịch chuyển khung nhìn bản đồ" data-html="true" data-placement="bottom">
                                                <i class="fa fa-hand-paper-o"></i>
                                            </a>
                                        </li>
                                    </div>
                                </div>
                            </div>
                            <div class="toolsBarRight">
                                <div class="listIcon">
                                    <div class="btn-group">
                                        <li class="item"><a id="toanmanhinh" class="tooltips" href="#" data-original-title="Chế độ toàn màn hình" data-html="true" data-placement="bottom"><span class="fa fa-arrows-alt changeIcon"></span><span class="default-color"></span></a></li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="mapdividContainer">
                <!-- map here -->
            </div>
        </div>
    </div>

    <script type="text/javascript" src="Assets/js/main.js"></script>
</body>

</asp:Content>
